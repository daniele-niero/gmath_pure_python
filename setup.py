import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="gmath",
    version="1.0.0",
    author="Daniele Niero",
    author_email="daniele.niero@gmail.com",
    description="Linear algebra library written in pure Python",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/daniele-niero/gmath_pure_python",
    project_urls={
        "Bug Tracker": "https://bitbucket.org/daniele-niero/gmath_pure_python/issues",
    },
    classifiers=[
        "Development Status :: 1 - Release"
        "Programming Language :: Python :: 2",
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    package_dir={"": "src"},
    packages=setuptools.find_packages(where="src"),
    python_requires=">=2.7, >=3.6",
)