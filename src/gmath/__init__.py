from __future__ import division, print_function

# raise ImportError('Pure Python GMath is not ready to be deployed')

__version__ = '2.0.py_dev'


import sys
import math
import six
import textwrap
import warnings
import inspect
import enum


from .DccSupport import *


if sys.version_info[0] == 2:
    # python 3 has this function in module math
    def isclose(a, b, rel_tol=1e-09, abs_tol=0.0):
        return abs(a-b) <= max(rel_tol * max(abs(a), abs(b)), abs_tol)
else:
    from math import isclose


#--------------------------------------------------------------------#
#                             Constants                              #
#--------------------------------------------------------------------#

PI        = 4.0*math.atan(1.0)
HALFPI    = PI*0.5
RADFACTOR = PI/180.0
DEGFACTOR = 180.0/PI
PRECISION = 1e-12

#--------------------------------------------------------------------#
#                            Enumerators                             #
#--------------------------------------------------------------------#

class CartesianPlane(enum.IntEnum):
    XY = 0
    YZ = 1
    ZX = 2
    YX = 0
    ZY = 1
    XZ = 2

    def normalVector(self):
        ''' return the vector perpendicular to this plane '''
        if self.value == 0:
            return Vector3.ZAXIS
        elif self.value == 1:
            return Vector3.XAXIS
        elif self.value == 2:
            return Vector3.YAXIS


class Unit(enum.IntEnum):
    Degrees = 0
    Radians = 1


class RotationOrder(enum.IntEnum):
    XYZ = 0
    XZY = 1
    YXZ = 2
    YZX = 3
    ZXY = 4
    ZYX = 5


class Axis(enum.IntEnum):
    NEGX = -1
    NEGY = -2
    NEGZ = -3
    POSX =  1
    POSY =  2
    POSZ =  3

    def asVector3(self):
        ''' Gets a Vector3 instance from the Axis enumerator value '''
        if self.value == 1:
            return Vector3.XAXIS
        elif self.value == 2:
            return Vector3.YAXIS
        elif self.value == 3:
            return Vector3.ZAXIS
        elif self.value == -1:
            return -Vector3.XAXIS
        elif self.value == -2:
            return -Vector3.YAXIS
        elif self.value == -3:
            return -Vector3.ZAXIS

    def isX(self):
        ''' Returns if Axis is the X axis (positive or negative) '''
        return abs(self.value) == 1

    def isY(self):
        ''' Returns if Axis is the Y axis (positive or negative) '''
        return abs(self.value) == 2

    def isZ(self):
        ''' Returns if Axis is the Z axis (positive or negative) '''
        return abs(self.value) == 3

#--------------------------------------------------------------------#
#                               Errors                               #
#--------------------------------------------------------------------#

class GMathError(ArithmeticError):
    pass


def pendingDeprecation(message):
    frame = inspect.stack()[-1]
    warnings.showwarning(
        message,
        PendingDeprecationWarning,
        frame[1],
        frame[2])

#--------------------------------------------------------------------#
#                             Utilities                              #
#--------------------------------------------------------------------#

def isAxisX(axis):
    pendingDeprecation('Deprecated in favour of using Axis enumerator itself: Axis.isX()')
    return axis.isX()

def isAxisY(axis):
    pendingDeprecation('Deprecated in favour of using Axis enumerator itself: Axis.isY()')
    return axis.isY()

def isAxisZ(axis):
    pendingDeprecation('Deprecated in favour of using Axis enumerator itself: Axis.isZ()')
    return axis.isZ()

def getVector3FromAxis(axis):
    pendingDeprecation('Deprecated in favour of using Axis enumerator itself. ex: Axis.POSX.asVector3()')
    return axis.asVector3()


def safeAcos(x):
    if (x >= -1.0):
        if (x <= 1.0):
            return math.acos(x)
        else:
            return 0.0
    else:
        return PI

def safeAsin(x):
    if (x >= -1.0):
        if (x <= 1.0):
            return math.asin(x)
        else:
            return HALFPI
    else:
        return -HALFPI


def sinXOverX(x):
    ''' Don Hatch's version of sin(x)/x, which is accurate for very
        small x. Taken from ILM's Imath

    Returns sin(x)/x or just 1.0 if x==0.0
    '''
    if isclose(x * x, 0.0, abs_tol=PRECISION):
        return 1.0
    else:
        return math.sin(x) / x


def toRadians(x):
    return x * RADFACTOR


def toDegrees(x):
    return x * DEGFACTOR


def _safeOrthogonalAxis(primary, secondary, tertiary, primaryAxis, secondaryAxis):
    ''' Ensure axis are Left-Hand compliant (or is it Right-Hand? Anyway, whatever Maya handiness is) '''
    if primaryAxis.isX():
        if secondaryAxis.isY():
            return (primary, secondary, tertiary)

        elif secondaryAxis.isZ():
            return (primary, -tertiary, secondary)

    elif primaryAxis.isY():
        if secondaryAxis.isX():
            return (secondary, primary, -tertiary)

        elif secondaryAxis.isZ():
            return (tertiary, primary, secondary)

    elif primaryAxis.isZ():
        if secondaryAxis.isX():
            return (secondary, tertiary, primary)

        elif secondaryAxis.isY():
            return (-tertiary, secondary, primary)


def _mirrorAxis(instance, mirrorNormal, primaryAxis, secondaryAxis):
    if isinstance(mirrorNormal, CartesianPlane):
        mirrorNormal = CartesianPlane.normalVector()

    primary = instance.getAxis(primaryAxis)
    secondary = instance.getAxis(secondaryAxis)

    primary.mirrorInPlace(mirrorNormal)
    secondary.mirrorInPlace(mirrorNormal)

    tertiary = primary.cross(secondary)

    orthoAxis = _safeOrthogonalAxis(primary, secondary, tertiary, primaryAxis, secondaryAxis)
    return orthoAxis[0].__data__() + orthoAxis[1].__data__() + orthoAxis[2].__data__()


def _getAimAxis(direction, upVector, primaryAxis, secondaryAxis):
    if isclose(direction.dot(upVector), 1.0, abs_tol=1e-09):
        raise GMathError("`direction` vector and `upVector` vector are parallel. Impossible to create a matrix out of them.")

    if primaryAxis < 0:
        direction *= -1.0

    if secondaryAxis < 0:
        upVector *= -1.0

    tertiary = direction.cross(upVector)
    upVector = tertiary.cross(direction)

    return _safeOrthogonalAxis(direction, upVector, tertiary, primaryAxis, secondaryAxis)


def aim(result, direction, upVector, primaryAxis, secondaryAxis):
    ''' Orient 'result' to point at 'direction'.

    The resulting transformation will have the primaryAxis points to target, and
    the secondaryAxis is as close as possible to the up vector.

    Args:
        result: One of these objects instance Matrix3, Matrix4, Quaternion, Xfo
        direction: A Vector3 holding the position to aim at.
        upVector: A Vector3 to define the roll the result object will have, around the direction axis
        primaryAxis: One of the value in Axis enumerator. Defines which axis will point to direction
        secondaryAxis: One of the value in Axis enumerator. Defines which axis will point to upVector
    '''
    axis = _getAimAxis(direction, upVector, primaryAxis, secondaryAxis)

    if isinstance(result, Matrix3):
        result.setRow(0, axis[0])
        result.setRow(1, axis[1])
        result.setRow(2, axis[2])
    else:
        mat = Matrix3(axis[0], axis[1], axis[2])

        if isinstance(result, (Quaternion, Matrix4)):
            result.fromMatrix3(mat)
        elif isinstance(result, Xfo):
            result.ori.setFromMatrix3(mat)


def _getFastAimAxis(direction, upVector):
    if isclose(direction.dot(upVector), 1.0, abs_tol=1e-09):
        raise GMathError("`direction` vector and `upVector` vector are parallel. Impossible to create a matrix out of them.")

    return (direction,
            primary.cross(upVector).cross(primary),
            secondary.cross(primary))


def fastAim(result, direction, upVector):
    ''' Orient 'result' to point at 'direction'.

    This is a fast version of `aim`, whichever object `result` is, it will have the Y axis
    pointing to 'direction' and the X axis pointing to 'upVector'.

    Args:
        result: One of these objects instance Matrix3, Matrix4, Quaternion, Xfo
        direction: A Vector3 holding the position to aim at.
        upVector: A Vector3 to define the roll the result object will have, around the direction axis
    '''
    primary, secondary, tertiary = _getFastAimAxis(direction, upVector)

    if isinstance(result, Matrix3):
        result.setRow(0, secondary)
        result.setRow(1, primary)
        result.setRow(2, tertiary)
    else:
        mat = Matrix3(secondary, primary, tertiary)

        if isinstance(result, (Quaternion, Matrix4)):
            result.fromMatrix3(mat)
        elif isinstance(result, Xfo):
            result.ori.setFromMatrix3(mat)


#--------------------------------------------------------------------#
#                            Math Classes                            #
#--------------------------------------------------------------------#

@six.add_metaclass(DccExpand)
class Base(object):
    ''' Common base class for the other Math classes.

    It provides all the common methods for data managements, converting to string and
    it carries the DccExpand metaclass
    '''

    def __init__(self, *args):
        self.__dataLength = len(args)
        self.__data = [0.0] * self.__dataLength

        if args:
            self.set(*args)


    def __str__(self):
        values = ["{:0.3f}".format(x).rstrip('0').rstrip('.') for x in self]
        result = ''
        if self.__dataLength == 9:
            step = 3

        elif self.__dataLength == 16:
            step = 4

        else:
            step = 1

        if step > 1:
            for i in range(step):
                result += '\n  ' + ', '.join(values[i*step:i*step+step])
        else:
            result = ', '.join(values)

        return 'gmath(py).{}({})'.format(self.__class__.__name__, result)


    def __repr__(self):
        return super(Base, self).__repr__().replace('gmath', 'gmath(py)')


    #---------------------- Arithmetic comparisons ----------------------#

    def __eq__(self, other):
        for i in range(len(self)):
            if not isclose(self.__data[i], other[i], abs_tol=PRECISION):
                return False
        return True


    def __neq__(self, other):
        return not self.__eq__(other)

    #--------------------------- Data access ----------------------------#

    def __len__(self):
        return self.__dataLength


    def __getitem__(self, index):
        return self.__data[index]


    def __setitem__(self, index, value):
        self.__data[index] = value


    def __iter__(self):
        for value in self.__data:
            yield value


    def __data__(self):
        ''' Returns a *reference* of the internal data. use with care! '''
        return self.__data


    def data(self):
        ''' Returns a copy of the internal data '''
        # always return a copy
        return [v for v in self.__data]


    def set(self, *args):
        '''Sets the internal data with a a sequence of floats, a list/tuple or with another instance of the same object

        Args:
            *args: (Instance, List/Tuple, sequence of floats)

        Raises:
            TypeError: If the wrong type is passed
            ValueError: if the argument passed is right but with the wrong length
        '''
        argsLen = len(args)
        if argsLen == 1:
            if not isinstance(args[0], (self.__class__, list, tuple)):
                raise TypeError(
                    'Wrong type ({wrong}) passed to {right}.set().\n'
                    'An instance of {right}, a list/tuple or a sequence of {length} floats is expected'.format(
                        wrong=args[0],
                        right=self.__class__.__name__,
                        length=self.__dataLength)
                )

            args = args[0]

        # no, it is not an elif
        if len(args) != self.__dataLength:
            raise ValueError('Received {} arguments (after unpacking) but {} are required'.format(len(args), self.__dataLength))

        for i in range(self.__dataLength):
            self.__data[i] = args[i]


    def toString(self):
        ''' Explicit call to str(self)

        String representation of this object
        '''
        return str(self)


class Vector3(Base):

    def __init__(self, *args):
        ''' It accepts:
        * 3 float values
        * a list of 3 float values
        * another Vector3
        '''

        alen = len(args)

        if alen == 0:
            super(Vector3, self).__init__(0.0, 0.0, 0.0)

        elif alen == 3:
            super(Vector3, self).__init__(*args)

        elif alen == 1 and isinstance(args[0], (Vector3, list, tuple)):
            super(Vector3, self).__init__(*args[0])

        else:
            raise TypeError('Impossible to set a Vector3 with this argument(s) - {} -'.format(args))

    #---------------------- Arithmetic operations -----------------------#

    def __neg__(self):
        return Vector3(-self.x, -self.y, -self.z)


    def __add__(self, other):
        return Vector3(self.x + other[0], self.y + other[1], self.z + other[2])


    def __sub__(self, other):
        return Vector3(self.x - other[0], self.y - other[1], self.z - other[2])


    def __mul__(self, other):
        if isinstance(other, (Vector3, list, tuple)):
            return Vector3(self.x * other[0], self.y * other[1], self.z * other[2])
        elif isinstance(other, (Matrix3, Matrix4, Quaternion)):
            return other.transformVector(self)
        else:
            return Vector3(self.x * other, self.y * other, self.z * other)


    def __div__(self, other):
        return self.__truediv__(other)


    def __truediv__(self, other):
        if isinstance(other, (Vector3, list, tuple)):
            return Vector3(self.x / other[0], self.y / other[1], self.z / other[2])
        else:
            return Vector3(self.x / other, self.y / other, self.z / other)

    #------------------------ Arithmetic updates ------------------------#

    def __iadd__(self, other):
        self.x += other[0]
        self.y += other[1]
        self.z += other[2]
        return self


    def __isub__(self, other):
        self.x -= other[0]
        self.y -= other[1]
        self.z -= other[2]
        return self


    def __imul__(self, other):
        if isinstance(other, (Vector3, list, tuple)):
            self.x *= other[0]
            self.y *= other[1]
            self.z *= other[2]
        elif isinstance(other, (Matrix3, Matrix4, Quaternion)):
            self.set(*other.transformVector(self))
        else:
            self.x *= other
            self.y *= other
            self.z *= other
        return self


    def __idiv__(self, other):
        return self.__itruediv__(other)


    def __itruediv__(self, other):
        if isinstance(other, (Vector3, list, tuple)):
            self.x /= other[0]
            self.y /= other[1]
            self.z /= other[2]
        else:
            self.x /= other
            self.y /= other
            self.z /= other
        return self

    #--------------------------- Data access ----------------------------#

    @property
    def x(self):
        return self[0]

    @x.setter
    def x(self, value):
        self[0] = value


    @property
    def y(self):
        return self[1]

    @y.setter
    def y(self, value):
        self[1] = value


    @property
    def z(self):
        return self[2]

    @z.setter
    def z(self, value):
        self[2] = value

    #----------------------------- methods ------------------------------#

    def cross(self, other):
        return Vector3(self[1]*other[2] - self[2]*other[1],
                       self[2]*other[0] - self[0]*other[2],
                       self[0]*other[1] - self[1]*other[0])


    def crossInPlace(self, other):
        newx = self.y*other.z - self.z*other.y
        newy = self.z*other.x - self.x*other.z
        newz = self.x*other.y - self.y*other.x
        self.x = newx
        self.y = newy
        self.z = newz
        return self


    def crossNormalize(self, other):
        r = Vector3(self[1]*other[2] - self[2]*other[1],
                    self[2]*other[0] - self[0]*other[2],
                    self[0]*other[1] - self[1]*other[0])
        r.normalizeInPlace()
        return r


    def crossNormalizeInPlace(self, other):
        self.crossInPlace(other)
        self.normalizeInPlace()
        return self


    def dot(self, other):
        return self[0]*other[0] + self[1]*other[1] + self[2]*other[2]


    def length(self):
        return math.sqrt(self.x*self.x + self.y*self.y + self.z*self.z)


    def squaredLength(self):
        return self.x*self.x + self.y*self.y + self.z*self.z


    def distance(self, other):
        r = Vector3(self - other)
        return r.length()


    def squaredDistance(self, other):
        r = Vector3(self - other)
        return r.squaredLength()


    def normalize(self):
        vlen = self.length()
        if isclose(vlen, 0.0):
            raise ZeroDivisionError("Cannot normalize a Vector3 or length = 0.0")
        else:
            data = self.__data__()
            return Vector3(data[0]/vlen, data[1]/vlen, data[2]/vlen)


    def normalizeInPlace(self):
        vlen = self.length()
        if not isclose(vlen, 0.0):
            self.x /= vlen
            self.y /= vlen
            self.z /= vlen
        return self


    def inverse(self):
        return Vector3(-self.x, -self.y, -self.z)


    def inverseInPlace(self):
        self.x = -self.x
        self.y = -self.y
        self.z = -self.z
        return self


    def angle(self, other):
        dividend = self.length() * other.length()
        if isclose(dividend, 0.0):
            return safeAcos( self.dot(other) )
        else:
            return safeAcos( self.dot(other) / dividend )


    def reflect(self, normal):
        '''
        Args:
            normal: Vector perpendicular to the reflection plane
                    Note: Be sure this is a normalized vector
        '''
        dot = self.dot(normal)
        return normal * (2.0*dot) - self


    def reflectInPlace(self, normal):
        '''
        Args:
            normal: Vector perpendicular to the reflection plane.
                    Note: Be sure this is a normalized vector
        '''
        dot = self.dot(normal)
        self.set(normal * (2.0*dot) - self)
        return self


    def refract(self, normal, eta):
        '''
        Args:
            normal: Vector perpendicular to the plane of refraction.
                    Note: Be sure this is a normalized vector
            eta: Refraction index
        '''
        dot = self.dot(normal)
        k = 1.0 - eta*eta * (1.0 - dot*dot)

        if not isclose(k, 0.0):
            retVec = self * eta
            retVec -= normal * (eta*dot + sqrt(k))
            return retVec
        else:
            return Vector3()


    def refractInPlace(self, normal, eta):
        '''
        Args:
            normal: Vector perpendicular to the plane of refraction.
                    Note: Be sure this is a normalized vector
            eta: Refraction index
        '''
        dot = self.dot(normal)
        k = 1.0 - eta*eta * (1.0 - dot*dot)

        if not isclose(k, 0.0):
            self *= eta
            self -= normal * (eta*dot + sqrt(k))
        else:
            self.set(0.0, 0.0, 0.0)

        return self


    def mirror(self, normal):
        ''' Mirror this vector along the mirror axis

        Args:
            normal: Vector or CartesianPlane enumerator.
                    `self` will be mirrored on the opposite direction of this axis
                    Note: When a vector, be sure it is normalized
        '''
        return Vector3(self).mirrorInPlace(normal)


    def mirrorInPlace(self, normal):
        ''' Mirror this vector along the mirror axis

        Args:
            normal: Vector or CartesianPlane enumerator.
                    `self` will be mirrored on the opposite direction of this axis
                    Note: When a vector, be sure it is normalized
        '''
        if isinstance(normal, CartesianPlane):
            normal = CartesianPlane.normalVector()

        u = normal * (self.dot(normal)) * 2.0
        self -= u
        return self


    def linearInterpolate(self, other, weight):
        return Vector3((other.x - self.x) * weight + self.x,
                       (other.y - self.y) * weight + self.y,
                       (other.z - self.z) * weight + self.z)


    def linearInterpolateInPlace(self, other, weight):
        self.x = (other.x - self.x) * weight + self.x
        self.y = (other.y - self.y) * weight + self.y
        self.z = (other.z - self.z) * weight + self.z
        return self

    #---------------------------- Deprecated ----------------------------#

    def negateInPlace(self):
        pendingDeprecation('Vector3.negateInPlace is deprecated because a simple duplicate of Vector3.inverseInPlace. Please use that instead')
        self.x = -self.x
        self.y = -self.y
        self.x = -self.x
        return self


    def negate(self):
        pendingDeprecation('Vector3.negate is deprecated because a simple duplicate of Vector3.inverse. Please use that instead')
        return -self


Vector3.ZERO  = Vector3(0.0, 0.0, 0.0)
Vector3.XAXIS = Vector3(1.0, 0.0, 0.0)
Vector3.YAXIS = Vector3(0.0, 1.0, 0.0)
Vector3.ZAXIS = Vector3(0.0, 0.0, 1.0)



class Vector4(Base):

    def __init__(self, *args):
        '''
        It accepts:
        * 4 float values
        * a list/tuple of 4 float values
        * another Vector4
        '''

        alen = len(args)

        if alen == 0:
            super(Vector4, self).__init__(0.0, 0.0, 0.0, 0.0)
        elif alen == 4:
            super(Vector4, self).__init__(*args)

        elif alen == 1 and isinstance(args[0], (Vector4, list, tuple)):
            super(Vector4, self).__init__(*args[0])

        else:
            raise TypeError('Impossible to set a Vector3 with this argument(s) - {} -'.format(args))

    #---------------------- Arithmetic operations -----------------------#

    def __neg__(self):
        return Vector4(-self.x, -self.y, -self.z, -self.w)


    def __add__(self, other):
        return Vector4(self.x + other.x, self.y + other.y, self.z + other.z, self.w + other.w)


    def __sub__(self, other):
        return Vector4(self.x - other.x, self.y - other.y, self.z - other.z, self.w - other.w)


    def __mul__(self, other):
        if isinstance(other, Vector4):
            return Vector4(self.x * other.x, self.y * other.y, self.z * other.z, self.w * other.w)
        else:
            return Vector4(self.x * other, self.y * other, self.z * other, self.w * other.w)


    def __div__(self, other):
        return self.__truediv__(other)


    def __truediv__(self, other):
        if isinstance(other, Vector4):
            return Vector4(self.x / other.x, self.y / other.y, self.z / other.z, self.w / other.w)
        else:
            return Vector4(self.x / other, self.y / other, self.z / other.z, self.w / other.w)

    #------------------------ Arithmetic updates ------------------------#

    def __iadd__(self, other):
        self.x += other.x
        self.y += other.y
        self.z += other.z
        self.w += other.w
        return self


    def __isub__(self):
        self.x -= self.x
        self.y -= self.y
        self.z -= self.z
        self.w -= other.w
        return self


    def __imul__(self, other):
        if isinstance(other, Vector4):
            self.x *= other.x
            self.y *= other.y
            self.z *= other.z
            self.w *= other.w
        else:
            self.x *= other
            self.y *= other
            self.z *= other
            self.w *= other
        return self


    def __idiv__(self, other):
        return self.__itruediv__(other)


    def __itruediv__(self, other):
        if isinstance(other, Vector4):
            self.x /= other.x
            self.y /= other.y
            self.z /= other.z
            self.w /= other.w
        else:
            self.x /= other
            self.y /= other
            self.z /= other
            self.w /= other
        return self

    #--------------------------- Data access ----------------------------#

    @property
    def x(self):
        return self[0]

    @x.setter
    def x(self, value):
        self[0] = value


    @property
    def y(self):
        return self[1]

    @y.setter
    def y(self, value):
        self[1] = value


    @property
    def z(self):
        return self[2]

    @z.setter
    def z(self, value):
        self[2] = value


    @property
    def w(self):
        return self[3]

    @w.setter
    def w(self, value):
        self[3] = value

    #----------------------------- methods ------------------------------#

    def dot(self, other):
        return self.x*other.x + self.y*other.y + self.z*other.z + self.w*other.w


    def length(self):
        return math.sqrt(self.x*self.x + self.y*self.y + self.z*self.z, self.w*self.w)


    def squaredLength(self):
        return self.x*self.x + self.y*self.y + self.z*self.z, self.w*self.w


    def normalize(self):
        vlen = self.length()
        if isclose(vlen, 0.0):
            return Vector4(self)
        else:
            return Vector4(x/vlen, y/vlen, z/vlen, w/vlen)


    def normalizeInPlace(self):
        vlen = self.length()
        if not isclose(vlen, 0.0):
            self.x /= vlen
            self.y /= vlen
            self.z /= vlen
            self.w /= vlen
        return self



class Matrix3(Base):

    def __init__(self, *args):
        '''
        It accepts:
        * another Matrix3
        * a Euler
        * a Quaternion
        * a Vector3 and a float (axis and angle in radians)
        * 3 floats as rotation x, y, z (in radians)
        * 3 Vector3 as matrix rows
        * 3 lists of 3 values as matrix rows
        * 9 float values
        * a list/tuple of 9 float values
        '''

        alen = len(args)

        # Resolvable with super init

        if alen == 9:
            super(Matrix3, self).__init__(*args)
            return

        elif alen == 1 and isinstance(args[0], (Matrix3, list, tuple)):
            super(Matrix3, self).__init__(*args[0])
            return

        # Initialise as default then see if post initialisation is required.

        super(Matrix3, self).__init__(1.0, 0.0, 0.0,
                                      0.0, 1.0, 0.0,
                                      0.0, 0.0, 1.0)

        if alen:
            if alen == 1 and isinstance(other, Euler):
                self.fromEuler(other)

            elif alen == 1 and isinstance(other, Quaternion):
                    self.fromQuaternion(other)

            elif alen == 2 and isinstance(args[0], Vector3) and isinstance(args[1], (float, int)):
                self.fromAxisAngle(args[0], args[1])

            elif alen == 3:
                if isinstance(args[0], (Vector3, list)) and isinstance(args[1], (Vector3, list)) and isinstance(args[2], (Vector3, list)):
                    for i in range(0, 3):
                        for j in range(0, 3):
                            self[i*3+j] = args[i][j]
                elif isinstance(args[0], float) and isinstance(args[1], float) and isinstance(args[2], float):
                    self.fromEuler(*args)

            else:
                raise AttributeError('Impossible to set a Matrix3 with this argument(s) - {} -'.format(args))

    #---------------------- Arithmetic comparisons ----------------------#

    def __eq__(self, other):
        for i in range(9):
            if not isclose(self[i], other[i]):
                return False
        return True


    def __neq__(self, other):
        return not self.__eq__(other)

    #---------------------- Arithmetic operations -----------------------#

    def __mul__(self, other):
        return Matrix3(
            self[0]*other[0] + self[1]*other[3] + self[2]*other[6],
            self[0]*other[1] + self[1]*other[4] + self[2]*other[7],
            self[0]*other[2] + self[1]*other[5] + self[2]*other[8],

            self[3]*other[0] + self[4]*other[3] + self[5]*other[6],
            self[3]*other[1] + self[4]*other[4] + self[5]*other[7],
            self[3]*other[2] + self[4]*other[5] + self[5]*other[8],

            self[6]*other[0] + self[7]*other[3] + self[8]*other[6],
            self[6]*other[1] + self[7]*other[4] + self[8]*other[7],
            self[6]*other[2] + self[7]*other[5] + self[8]*other[8]
            )

    #------------------------ Arithmetic updates ------------------------#

    def __imul__(self, other):
        self.set(
            self.data[0]*other[0] + self[1]*other[3] + self[2]*other[6],
            self.data[0]*other[1] + self[1]*other[4] + self[2]*other[7],
            self.data[0]*other[2] + self[1]*other[5] + self[2]*other[8],

            self.data[3]*other[0] + self[4]*other[3] + self[5]*other[6],
            self.data[3]*other[1] + self[4]*other[4] + self[5]*other[7],
            self.data[3]*other[2] + self[4]*other[5] + self[5]*other[8],

            self.data[6]*other[0] + self[7]*other[3] + self[8]*other[6],
            self.data[6]*other[1] + self[7]*other[4] + self[8]*other[7],
            self.data[6]*other[2] + self[7]*other[5] + self[8]*other[8]
            )
        return self

    #--------------------------- Data access ----------------------------#

    def __call__(self, row, col):
        ''' Get the value of the internal data for the specified row at specified column

        Args:
            row: (int) The row index
            col: (int) The column index

        Returns:
            float value

        Raises:
            IndexError: if row or column are outside the allowed range (0, 2)
        '''
        if row < 0 or row > 2:
            raise IndexError('Index for row is invalid ({})'.format(row))

        if col < 0 or col > 2:
            raise IndexError('Index for column is invalid ({})'.format(col))

        return self[3*row + col]

    #----------------------------- methods ------------------------------#

    def setToIdentity(self):
        self.set(1.0, 0.0, 0.0,
                 0.0, 1.0, 0.0,
                 0.0, 0.0, 1.0)
        return self


    def getColumn(self, i):
        return Vector3(self[i], self[i+3], self[i+6])


    def setColumn(self, i, vec):
        ''' Set the specific matrix column.

        Args:
            i: (int) Column index
            vec: Vector3 or any iterable of floats with at least 3 values
        '''
        self[colIndex]   = vec[0]
        self[colIndex+3] = vec[1]
        self[colIndex+6] = vec[2]


    def getRow(self, i):
        return Vector3(self[i*3 : i*3+3])


    def setRow(self, i, vec):
        ''' Set the specific matrix row.

        Args:
            i: (int) Row index
            vec: Vector3 or any iterable of floats with at least 3 values
        '''
        self[i*3]   = vec[0]
        self[i*3+1] = vec[1]
        self[i*3+2] = vec[2]


    def getAxis(self, axis):
        ''' Gets a Vector3 representing one axis of this matrix.

        The negative of an axis can also be asked.

        Args:
            axis: One of the possible values in Axis enumerator
        '''
        if axis > 0:
            return self.getRow(axis.value - 1)
        else:
            return -self.getRow(axis.value - 1)


    def getAxisX(self):
        ''' see setRow '''
        return self.getRow(0)


    def getAxisY(self):
        ''' see setRow '''
        return self.getRow(1)


    def getAxisZ(self):
        ''' see setRow '''
        return self.getRow(2)


    def setAxisX(self, vec):
        ''' see setRow '''
        self.setRow(0, vec)


    def setAxisY(self, vec):
        ''' see setRow '''
        self.setRow(1, vec)


    def setAxisZ(self, vec):
        ''' see setRow '''
        self.setRow(2, vec)


    def setScale(self, *value):
        '''
        It accepts:
        * tuple (of 3 values)
        * list  (of 3 values)
        * Vector3
        * 3 floats
        '''

        if len(value) == 1:
            # assume iterable with at least 3 values
            value = value[0]

        self.setRow(0, self.getRow(0) * value[0])
        self.setRow(1, self.getRow(1) * value[1])
        self.setRow(2, self.getRow(2) * value[2])


    def addScale(self, *value):
        '''
        It accepts:
        * tuple (of 3 values)
        * list  (of 3 values)
        * Vector3
        * 3 floats
        '''

        if len(value) == 1:
            # assume iterable with at least 3 values
            value = value[0]

        x = [v + value[0] for v in self.getRow(0)]
        y = [v + value[1] for v in self.getRow(1)]
        z = [v + value[2] for v in self.getRow(2)]

        self.setRow(0, x)
        self.setRow(1, y)
        self.setRow(2, z)


    def getScale(self):
        return Vector3(self.getRow(0).lenght(), self.getRow(1).lenght(), self.getRow(2).lenght())


    def transpose(self):
        return Matrix3(
            self[0], self[3], self[6],
            self[1], self[4], self[7],
            self[2], self[5], self[8] )


    def transposeInPlace(self):
        self.set(
            self[0], self[3], self[6],
            self[1], self[4], self[7],
            self[2], self[5], self[8] )
        return self


    def determinant(self):
        return self[0] * ( self[4] * self[8] - self[7] * self[5] ) - \
               self[1] * ( self[3] * self[8] - self[6] * self[5] ) + \
               self[2] * ( self[3] * self[7] - self[6] * self[4] )


    def inverse(self):
        retMatrix = Matrix3(self)
        retMatrix.inverseInPlace()
        return retMatrix


    def inverseInPlace(self):
        invDet = 1.0/self.determinant()
        newData = [0.0] * 9

        if not isclose(invDet, 0.0):
            newData[0] =   self[4]*self[8] - self[5]*self[7]  / invDet
            newData[1] = -(self[1]*self[8] - self[7]*self[2]) / invDet
            newData[2] =   self[1]*self[5] - self[4]*self[2]  / invDet

            newData[3] = -(self[3]*self[8] - self[5]*self[6]) / invDet
            newData[4] =   self[0]*self[8] - self[6]*self[2]  / invDet
            newData[5] = -(self[0]*self[5] - self[3]*self[2]) / invDet

            newData[6] =   self[3]*self[7] - self[6]*self[4]  / invDet
            newData[7] = -(self[0]*self[7] - self[6]*self[1]) / invDet
            newData[8] =   self[0]*self[4] - self[1]*self[3]  / invDet

        self.set(newData)
        return self


    def fromQuaternion(self, quat):
        self.set(quat._toMatrixData())


    def toQuaternion(self):
        return Quaternion(self)


    def fromEuler(self, eulerAngles, rotationOrder=RotationOrder.XYZ):
        ''' Set this matrix with the euler angles given.

        Args:
            eulerAngles: (Euler or iterable). The Euler angles, can be a Euler instance or any iterable with at least 3 floats.
                         When passing an iterable, the values are considered always in *radians*.
            rotationOrder: The rotation order the Euler are expressed with. (default: {RotationOrder.XYZ})

        Raises:
            AttributeError: Raised when the eulerAngle attribute is not an Euler instance or list/tuple
        '''
        if isinstance(eulerAngles, Euler):
            # ensure euler is radians
            r = eulerAngles.toRadians();
            angleX, angleY, angleZ = eulerAngles.data()
        else:
            angleX, angleY, angleZ = eulerAngles

        cx = math.cos(angleX)
        sx = math.sin(angleX)
        cy = math.cos(angleY)
        sy = math.sin(angleY)
        cz = math.cos(angleZ)
        sz = math.sin(angleZ)

        XMat = Matrix3(
            1.0, 0.0, 0.0,
            0.0,  cx,  sx,
            0.0, -sx,  cx);

        YMat = Matrix3(
             cy, 0.0, -sy,
            0.0, 1.0, 0.0,
             sy, 0.0,  cy);

        ZMat = Matrix3(
             cz,  sz, 0.0,
            -sz,  cz, 0.0,
            0.0, 0.0, 1.0);

        if rotationOrder == RotationOrder.XYZ:
            self.set(XMat * (YMat*ZMat))

        elif rotationOrder == RotationOrder.XZY:
            self.set(XMat * (ZMat*YMat))

        elif rotationOrder == RotationOrder.YXZ:
            self.set(YMat * (XMat*ZMat))

        elif rotationOrder == RotationOrder.YZX:
            self.set(YMat * (ZMat*XMat))

        elif rotationOrder == RotationOrder.ZXY:
            self.set(ZMat * (XMat*YMat))

        elif rotationOrder == RotationOrder.ZYX:
            self.set(ZMat * (YMat*XMat))


    def toEuler(self, rotationOrder=RotationOrder.XYZ):
        ''' Convert this Matrix3 to an Euler object.

        Args:
            rotationOrder: Rotation order for the Euler returned. (default: {RotationOrder.XYZ})

        Returns:
            Euler
        '''
        data = self.data()
        euler = Euler()

        if rotationOrder == RotationOrder.XYZ:
            if (data[6] < 1.0):
                if (data[6] > -1.0):
                    # y_angle = safeAsin(r02)
                    # x_angle = math.atan2(-r12, r22)
                    # z_angle = math.atan2(-r01, r00)
                    euler.y = safeAsin(data[6])
                    euler.x = math.atan2(-data[7], data[8])
                    euler.z = math.atan2(-data[3], data[0])
                else:
                    # y_angle = -PI/2
                    # z_angle - x_angle = math.atan2(r10,r11)
                    # WARNING.  The solution is not unique.  Choosing z_angle = 0.
                    euler.y = -HALFPI
                    euler.x = -math.math.atan2(data[1], data[4])
                    euler.z = 0.0
            else:
                # y_angle = +PI/2
                # z_angle + x_angle = math.atan2(r10, r11)
                # WARNING.  The solutions is not unique.  Choosing z_angle = 0.
                euler.y = HALFPI
                euler.x = math.math.atan2(data[1], data[4])
                euler.z = 0.0

        elif rotationOrder == RotationOrder.XZY:
            if (data[3] < 1.0):
                if (data[3] > -1.0):
                    # z_angle = safeAsin(-r01)
                    # x_angle = math.atan2(r21, r11)
                    # y_angle = math.atan2(r02, r00)
                    euler.z = safeAsin(-data[3])
                    euler.x = maath.math.atan2(data[5], data[4])
                    euler.y = maath.math.atan2(data[6], data[0])
                else:
                    # z_angle = +PI/2
                    # y_angle - x_angle = math.atan2(-r20, r22)
                    # WARNING.  The solution is not unique.  Choosing y_angle = 0.
                    euler.z = HALFPI
                    euler.x = -math.math.atan2(-data[2], data[8])
                    euler.y = 0.0
            else:
                # z_angle = -PI/2
                # y_angle + x_angle = math.atan2(-r20, r22)
                # WARNING.  The solution is not unique.  Choosing y_angle = 0.
                euler.z = HALFPI
                euler.x = math.math.atan2(-data[2], data[8])
                euler.y = 0.0

        elif rotationOrder == RotationOrder.YXZ:
            if (data[7] < 1.0):
                if (data[7] > -1.0):
                    # x_angle = safeAsin(-r12)
                    # y_angle = math.atan2(r02, r22)
                    # z_angle = math.atan2(r10, r11)
                    euler.x = safeAsin(-data[7])
                    euler.y = math.atan2(data[2], data[8])
                    euler.z = math.atan2(data[1], data[4])
                else:
                    # x_angle = +gmath.PI/2
                    # z_angle - y_angle = math.atan2(-r01,r00)
                    # WARNING.  The solution is not unique.  Choosing z_angle = 0.
                    euler.x = HALFPI
                    euler.y = -math.atan2(-data[3], data[0])
                    euler.z = 0.0
            else:
                # x_angle = -gmath.PI/2
                # z_angle + y_angle = math.atan2(-r01, r00)
                # WARNING.  The solution is not unique.  Choosing z_angle = 0.
                euler.x = -HALFPI
                euler.y = math.atan2(-data[3], data[0])
                euler.z = 0.0

        elif rotationOrder == RotationOrder.YZX:
            if (data[1] < 1.0):
                if (data[1] > -1.0):
                    # z_angle = safeAsin(r10)
                    # y_angle = math.atan2(-r20, r00)
                    # x_angle = math.atan2(-r12, r11)
                    euler.z = safeAsin(data[1])
                    euler.y = math.atan2(-data[2], data[0])
                    euler.x = math.atan2(-data[7], data[4])
                else:
                    # z_angle = -gmath.PI/2
                    # x_angle - y_angle = math.atan2(r21, r22)
                    # WARNING.  The solution is not unique.  Choosing x_angle = 0.
                    euler.z = -HALFPI
                    euler.y = -math.atan2(data[5], data[8])
                    euler.x = 0.0
            else:
                # z_angle = +PI/2
                # x_angle + y_angle = math.atan2(r21, r22)
                # WARNING.  The solution is not unique.  Choosing x_angle = 0.
                euler.z = HALFPI
                euler.y = math.atan2(data[5], data[8])
                euler.x = 0.0

        elif rotationOrder == RotationOrder.ZXY:
            if (data[5] < 1.0):
                if (data[5] > -1.0):
                    # x_angle = safeAsin(r21)
                    # z_angle = math.atan2(-r01, r11)
                    # y_angle = math.atan2(-r20, r22)
                    euler.x = safeAsin(data[5])
                    euler.z = math.atan2(-data[3], data[4])
                    euler.y = math.atan2(-data[2], data[8])
                else:
                    # x_angle = -PI/2
                    # y_angle - z_angle = math.atan2(r02, r00)
                    # WARNING.  The solution is not unique.  Choosing y_angle = 0.
                    euler.x = -HALFPI
                    euler.z = -math.atan2(data[6], data[0])
                    euler.y = 0.0
            else:
                # x_angle = +PI/2
                # y_angle + z_angle = math.atan2(r02, r00)
                # WARNING.  The solution is not unique.  Choosing y_angle = 0.
                euler.x = HALFPI
                euler.z = math.atan2(data[6], data[0])
                euler.y = 0.0

        elif rotationOrder == RotationOrder.ZYX:
            if (data[2] < 1.0):
                if (data[2] > -1.0):
                    # y_angle = safeAsin(-r20)
                    # z_angle = math.atan2(r10, r00)
                    # x_angle = math.atan2(r21, r22)
                    euler.y = safeAsin(-data[2])
                    euler.z = math.atan2(data[1], data[0])
                    euler.x = math.atan2(data[5], data[8])
                else:
                    # y_angle = +PI/2
                    # x_angle - z_angle = math.atan2(r01, r02)
                    # WARNING.  The solution is not unique.  Choosing x_angle = 0.
                    euler.y = HALFPI
                    euler.z = -math.atan2(data[3], data[6])
                    euler.x = 0.0
            else:
                # y_angle = -PI/2
                # x_angle + z_angle = math.atan2(-r01, -r02)
                # WARNING.  The solution is not unique.  Choosing x_angle = 0;
                euler.y = -HALFPI
                euler.z = math.atan2(-data[3], -data[6])
                euler.x = 0.0

        return euler


    def orthogonal(self):
        return Matrix3(Matrix3._orthogonalizeData(self.data()))


    def orthogonalInPlace(self):
        Matrix3._orthogonalizeData(self.__data__())


    def transformVector(self, vec):
        return Vector3(
            self[0]*vec[0] + self[3]*vec[1] + self[6]*vec[2],
            self[1]*vec[0] + self[4]*vec[1] + self[7]*vec[2],
            self[2]*vec[0] + self[5]*vec[1] + self[8]*vec[2]
        )


    def fromVectorToVector(self, fromVec, toVec):
        ''' Returns a rotation matrix that rotates one vector into another.

        The generated rotation matrix will rotate the vector `fromVec` into
        the vector `toVec`. `fromVec` and `toVec` must be unit vectors!

        This method is based on the code from:
            Tomas Moller, John Hughes
            Efficiently Building a Matrix to Rotate One Vector to Another
            Journal of Graphics Tools, 4(4):1-4, 1999
            http://www.acm.org/jgt/papers/MollerHughes99/

        Args:
            fromVector: Vector3 or iterable
            toVec: Vector3 or iterable
        '''
        self.set(*Matrix3._datafromVectorToVector(fromVec, toVec))


    def fromAxisAngle(self, axis, angle):
        self.set(*Matrix3._dataFromAxisAngle(axis, angle))


    def mirrorInPlace(self, normal, primaryAxis, secondaryAxis):
        self.set(_mirrorAxis(self, normal, primaryAxis, secondaryAxis))
        return self


    def mirror(self, normal, primaryAxis, secondaryAxis):
        return Matrix3(self).mirrorInPlace(normal, primaryAxis, secondaryAxis)


    def lookAt(self, pointAt, normal, primaryAxis, secondaryAxis):
        axis = _getAimAxis(pointAt, normal, primaryAxis, secondaryAxis)

        self.setRow(0, axis[0])
        self.setRow(1, axis[1])
        self.setRow(2, axis[2])


    def fastLookAt(self, pointAt, normal):
        axis = _getFastAimAxis(pointAt, normal)

        self.setRow(0, axis[0])
        self.setRow(1, axis[1])
        self.setRow(2, axis[2])


    @staticmethod
    def createLookAt(pointAt, normal, primaryAxis, secondaryAxis):
        mat = Matrix3()
        mat.lookAt(pointAt, normal, primaryAxis, secondaryAxis)
        return mat


    @staticmethod
    def _dataFromAxisAngle(axis, angle):
        sqr_a = axis[0] * axis[0]
        sqr_b = axis[1] * axis[1]
        sqr_c = axis[2] * axis[2]
        len2  = sqr_a + sqr_b + sqr_c

        k2    = math.cos(angle)
        k1    = (1.0 - k2) / len2
        k3    = math.sin(angle) / math.sqrt(len2)
        k1ab  = k1 * axis[0] * axis[1]
        k1ac  = k1 * axis[0] * axis[2]
        k1bc  = k1 * axis[1] * axis[2]
        k3a   = k3 * axis[0]
        k3b   = k3 * axis[1]
        k3c   = k3 * axis[2]

        return [
            k1*sqr_a + k2,
            k1ab + k3c,
            k1ac - k3b,

            k1ab - k3c,
            k1*sqr_b + k2,
            k1bc + k3a,

            k1ac + k3b,
            k1bc - k3a,
            k1*sqr_c + k2
        ]

    @staticmethod
    def _datafromVectorToVector(fromVec, toVec):
        ''' Returns a rotation matrix that rotates one vector into another.

        The generated rotation matrix will rotate the vector `fromVec` into
        the vector `toVec`. `fromVec` and `toVec` must be unit vectors!

        This method is based on the code from:

        Tomas Moller, John Hughes
        Efficiently Building a Matrix to Rotate One Vector to Another
        Journal of Graphics Tools, 4(4):1-4, 1999
        http://www.acm.org/jgt/papers/MollerHughes99/
        '''
        x = Vector3()
        u = Vector3()
        v = Vector3()

        e = fromVec.dot(toVec)
        f = math.fabs(e)


        if isclose(f, 1.0): # "from" and "to" vectors are parallel or almost parallel
            fx = abs(fromVec.x)
            fy = abs(fromVec.y)
            fz = abs(fromVec.z)

            if fx < fy:
                if fx < fz:
                    x.set(1.0, 0.0, 0.0)
                else:
                    x.set(0.0, 0.0, 1.0)
            else:
                if fy < fz:
                    x.set(0.0, 1.0, 0.0)
                else:
                    x.set(0.0, 0.0, 1.0)

            u = x - fromVec
            v = x - toVec

            c1 = 2.0 / u.dot(u)
            c2 = 2.0 / v.dot(v)
            c3 = v.dot(u * (c1*c2))

            uvals = [0.0] * 3
            vvals - [0.0] * 3
            uvals[0] = u.x
            uvals[1] = u.y
            uvals[2] = u.z
            vvals[0] = v.x
            vvals[1] = v.y
            vvals[2] = v.z

            result = [0.0] * 9
            for i in range(3):
                for j in range(3):
                    result[i*4 + j] = -c1*uvals[i]*uvals[j] - c2*vvals[i]*vvals[j] + c3*vvals[i]*uvals[j]

        else:  # the most common case, unless "from"="to", or "from"=-"to"
            v = fromVec.cross(toVec)
            h = 1.0/(1.0 + e)
            hvx = h * v.x
            hvz = h * v.z
            hvxy = hvx * v.y
            hvxz = hvx * v.z
            hvyz = hvz * v.y

            result = [
                e + hvx*v.x,
                hvxy - v.z,
                hvxz + v.y,

                hvxy + v.z,
                e + h*v.y*v.y,
                hvyz - v.x,

                hvxz - v.y,
                hvyz + v.x,
                e + hvz*v.z
            ]

        return result


    @staticmethod
    def _orthogonalizeData(data):
        # Code take it from WildMagic 5  -  www.geometrictools.com  -  here the matrix is transpose
        # Algorithm uses Gram-Schmidt orthogonalization.  If 'this' matrix is
        # M = [m0|m1|m2], then orthonormal output matrix is Q = [q0|q1|q2],

        #   q0 = m0/|m0|
        #   q1 = (m1-(q0*m1)q0)/|m1-(q0*m1)q0|
        #   q2 = (m2-(q0*m2)q0-(q1*m2)q1)/|m2-(q0*m2)q0-(q1*m2)q1|

        # where |V| indicates length of vector V and A*B indicates dot
        # product of vectors A and B.

        # Compute q0. length xAxis

        invLength = 1.0 / math.sqrt(data[0]*data[0] + data[1]*data[1] + data[2]*data[2])

        data[0] *= invLength
        data[1] *= invLength
        data[2] *= invLength

        # Compute q1.
        dot0 = data[0]*data[3] + data[1]*data[4] + data[2]*data[5]

        data[3] -= dot0 * data[0]
        data[4] -= dot0 * data[1]
        data[5] -= dot0 * data[2]

        invLength = 1.0 / math.sqrt(data[3]*data[3] + data[4]*data[4] + data[5]*data[5])

        data[3] *= invLength
        data[4] *= invLength
        data[5] *= invLength

        # compute q2
        dot1 = data[3]*data[6] + data[4]*data[7] + data[5]*data[8]

        dot0 = data[0]*data[6] + data[1]*data[7] + data[2]*data[8]

        data[6] -= dot0*data[0] + dot1*data[3]
        data[7] -= dot0*data[1] + dot1*data[4]
        data[8] -= dot0*data[2] + dot1*data[5]

        invLength = 1.0 / math.sqrt(data[6]*data[6] + data[7]*data[7] + data[8]*data[8])

        data[6] *= invLength
        data[7] *= invLength
        data[8] *= invLength

        return data


class Matrix4(Base):

    def __init__(self, *args):
        '''
        It accepts:
        * another Matrix4
        * a Matrix3
        * a Quaternion
        * a Euler

        * a Matrix3/Quaternion/Euler as rotation and a (Vector3/Vector4/list/tuple) as translation

        * 4 Vector3 as matrix rows
        * 4 vector4 as matrix rows
        * 4 lists/tuples of 4 values as matrix rows

        * 16 float values
        '''

        alen = len(args)

        # Resolvable with super init
        if alen == 16:
            super(Matrix4, self).__init__(*args)
            return

        elif alen == 1 and isinstance(args[0], (Matrix4, list, tuple)):
            super(Matrix4, self).__init__(*args[0])
            return

        # Initialise as default then see if post initialisation is required.
        super(Matrix4, self).__init__(1.0, 0.0, 0.0, 0.0,
                                      0.0, 1.0, 0.0, 0.0,
                                      0.0, 0.0, 1.0, 0.0,
                                      0.0, 0.0, 0.0, 1.0)
        if alen:
            if alen == 1 and isinstance(args[0], (Matrix3, Euler, Quaternion)):
                self.setRotation(args[0])

            elif alen == 2 and isinstance(args[0], (Matrix3, Euler, Quaternion)) and isinstance(args[1], (Vecotr3, Vector4, list, tuple)):
                self.setRotation(args[0])
                self.setPosition(args[1])

            elif alen == 4:
                self.setRow(0, args[0])
                self.setRow(1, args[1])
                self.setRow(2, args[2])
                self.setRow(3, args[3])


            else:
                raise AttributeError('Impossible to set a Matrix4 with this argument(s) - {} -'.format(args))

    #---------------------- Arithmetic operations -----------------------#

    def __mul__(self, other):
        return Matrix4(
            self[0]*other[0] + self[1]*other[4] + self[2]*other[8]  + self[3]*other[12],
            self[0]*other[1] + self[1]*other[5] + self[2]*other[9]  + self[3]*other[13],
            self[0]*other[2] + self[1]*other[6] + self[2]*other[10] + self[3]*other[14],
            self[0]*other[3] + self[1]*other[7] + self[2]*other[11] + self[3]*other[15],

            self[4]*other[0] + self[5]*other[4] + self[6]*other[8]  + self[7]*other[12],
            self[4]*other[1] + self[5]*other[5] + self[6]*other[9]  + self[7]*other[13],
            self[4]*other[2] + self[5]*other[6] + self[6]*other[10] + self[7]*other[14],
            self[4]*other[3] + self[5]*other[7] + self[6]*other[11] + self[7]*other[15],

            self[8]*other[0] + self[9]*other[4] + self[10]*other[8]  + self[11]*other[12],
            self[8]*other[1] + self[9]*other[5] + self[10]*other[9]  + self[11]*other[13],
            self[8]*other[2] + self[9]*other[6] + self[10]*other[10] + self[11]*other[14],
            self[8]*other[3] + self[9]*other[7] + self[10]*other[11] + self[11]*other[15],

            self[12]*other[0] + self[13]*other[4] + self[14]*other[8]  + self[15]*other[12],
            self[12]*other[1] + self[13]*other[5] + self[14]*other[9]  + self[15]*other[13],
            self[12]*other[2] + self[13]*other[6] + self[14]*other[10] + self[15]*other[14],
            self[12]*other[3] + self[13]*other[7] + self[14]*other[11] + self[15]*other[15]
            )

    #------------------------ Arithmetic updates ------------------------#

    def __imul__(self, other):
        self.set(
            self[0]*other[0] + self[1]*other[4] + self[2]*other[8]  + self[3]*other[12],
            self[0]*other[1] + self[1]*other[5] + self[2]*other[9]  + self[3]*other[13],
            self[0]*other[2] + self[1]*other[6] + self[2]*other[10] + self[3]*other[14],
            self[0]*other[3] + self[1]*other[7] + self[2]*other[11] + self[3]*other[15],

            self[4]*other[0] + self[5]*other[4] + self[6]*other[8]  + self[7]*other[12],
            self[4]*other[1] + self[5]*other[5] + self[6]*other[9]  + self[7]*other[13],
            self[4]*other[2] + self[5]*other[6] + self[6]*other[10] + self[7]*other[14],
            self[4]*other[3] + self[5]*other[7] + self[6]*other[11] + self[7]*other[15],

            self[8]*other[0] + self[9]*other[4] + self[10]*other[8]  + self[11]*other[12],
            self[8]*other[1] + self[9]*other[5] + self[10]*other[9]  + self[11]*other[13],
            self[8]*other[2] + self[9]*other[6] + self[10]*other[10] + self[11]*other[14],
            self[8]*other[3] + self[9]*other[7] + self[10]*other[11] + self[11]*other[15],

            self[12]*other[0] + self[13]*other[4] + self[14]*other[8]  + self[15]*other[12],
            self[12]*other[1] + self[13]*other[5] + self[14]*other[9]  + self[15]*other[13],
            self[12]*other[2] + self[13]*other[6] + self[14]*other[10] + self[15]*other[14],
            self[12]*other[3] + self[13]*other[7] + self[14]*other[11] + self[15]*other[15]
            )
        return self

    #--------------------------- Data Access ----------------------------#

    def __call__(self, row, col):
        ''' Get the value of the internal data for the specified row at specified column

        Args:
            row: (int) The row index
            col: (int) The column index

        Returns:
            float value

        Raises:
            IndexError: if row or cocollumn are outside the allowed range (0, 3)
        '''
        if row < 0 or row > 3:
            raise IndexError('Index for row is invalid ({})'.format(row))

        if col < 0 or col > 3:
            raise IndexError('Index for column is invalid ({})'.format(col))

        return self[4*row + col]


    def getAxisX(self):
        return Vector3(self[0 : 3])


    def getAxisY(self):
        return Vector3(self[4 : 7])


    def getAxisZ(self):
        return Vector3(self[8 : 11])


    def setAxisX(self, *args):
        self.setRow(0, *args)


    def setAxisY(self, *args):
        self.setRow(1, *args)


    def setAxisZ(self, *args):
        self.setRow(2, *args)


    def getRow(self, i):
        return Vector4(self[i*4 : i*4 + 4])


    def setRow(self, i, vec):
        mi = i*4
        for vi, v in enumerate(vec):
            self[mi + vi] = v


    def getColumn(self, i):
        return Vector4(self[i], self[i+4], self[i+8], self[i+12])


    def setColumn(self, i, vec):
        self[i] = vec[0]
        self[i+4] = vec[1]
        self[i+8] = vec[2]
        if len(vec) > 3:
            self[i+12] = vec[3]


    def __setRotationData(self, inData):
        self[0] = inData[0]
        self[1] = inData[1]
        self[2] = inData[2]
        self[3] = 0.0

        self[4] = inData[3]
        self[5] = inData[4]
        self[6] = inData[5]
        self[7] = 0.0

        self[8] = inData[6]
        self[9] = inData[7]
        self[10] = inData[8]
        self[11] = 0.0

    #----------------------------- Methods ------------------------------#

    def getPosition(self):
        return Vector3(self[12], self[13], self[14])


    def setPosition(self, *args):
        if len(args) == 1:
            args = args[0]

        self[12] = args[0]
        self[13] = args[1]
        self[14] = args[2]


    def addPosition(self, *args):
        if len(args) == 1:
            args = args[0]

        self[12] += args[0]
        self[13] += args[1]
        self[14] += args[2]


    def translate(self, *args):
        ''' Set the position components of this matrix following its own axis,
            as opposed to `setPosition` which will "move" the matrix accordingly to its
            reference space (parent/world)
        '''

        if len(args) == 1:
            args = args[0]

        self[12] += args[0]*self[0] + args[1]*self[4] + args[2]*self[8];
        self[13] += args[0]*self[1] + args[1]*self[5] + args[2]*self[9];
        self[14] += args[0]*self[2] + args[1]*self[6] + args[2]*self[10];


    def getScale(self):
        return Vector3(self.getAxisX().lenght(),
                       self.getAxisY().length(),
                       self.getAxisZ().length())


    def setScale(self, *args):
        if len(args) == 1:
            args = args[0]

        x = self.getAxisX().normalizeInPlace() * args[0]
        y = self.getAxisY().normalizeInPlace() * args[1]
        z = self.getAxisZ().normalizeInPlace() * args[2]

        self[0] = x[0]; self[1] = x[1]; self[2] =  x[2]
        self[4] = y[0]; self[5] = y[1]; self[6]  = y[2]
        self[8] = z[0]; self[9] = z[1]; self[10] = z[2]


    def addScale(self, *args):
        if len(args) == 1:
            args = args[0]

        self[0] += args[0]; self[1] += args[0]; self[2]  += args[0]
        self[4] += args[1]; self[5] += args[1]; self[6]  += args[1]
        self[8] += args[2]; self[9] += args[2]; self[10] += args[2]


    def setRotation(self, *args):
        if isinstance(args[0], Quaternion):
            self.fromQuaternion(args[0])

        elif isinstance(args[0], Matrix3):
            self.fromMatrix3(args[0])

        else:
            self.fromEuler(*args)


    def fromEuler(self, *inEuler):
        mat3 = Matrix3()
        mat3.fromEuler(*inEuler)
        self.fromMatrix3(mat3)


    def fromMatrix3(self, inMat3):
        '''Set the rotation part of this Matrix4 with the given Matrix3

        Note that only the rotation part is touched by this method, leaving all the other
        elements of the internal data as they are. This means that translation is not
        affected by this method.

        Args:
            rotationMatrix: A Matrix3 instance
        '''
        self.__setRotationData(inMat3)


    def fromQuaternion(self, inQuat):
        self.__setRotationData(inQuat._toMatrixData())


    def fromAxisAngle(self, axis, angle):
        matData = Matrix3._dataFromAxisAngle(axis, angle)
        self.__setRotationData(matData)


    def fromVectorToVector(self, fromVec, toVec):
        matData = Matrix3._datafromVectorToVector(fromVec, toVec)
        self.__setRotationData(matData)


    def toEuler(self, order=RotationOrder.XYZ):
        return self.toMatrix3().toEuler(order)


    def toMatrix3(self):
        return Matrix3(self[0], self[1], self[2],
                       self[4], self[5], self[6],
                       self[8], self[9], self[10])


    def toQuaternion(self):
        quat = Quaternion()
        quat.fromMatrix3(self)
        return quat


    def __determinantStep1(self):
        return (
            self[ 0]*self[ 5] - self[ 1]*self[ 4],
            self[ 0]*self[ 6] - self[ 2]*self[ 4],
            self[ 0]*self[ 7] - self[ 3]*self[ 4],
            self[ 1]*self[ 6] - self[ 2]*self[ 5],
            self[ 1]*self[ 7] - self[ 3]*self[ 5],
            self[ 2]*self[ 7] - self[ 3]*self[ 6],
            self[ 8]*self[13] - self[ 9]*self[12],
            self[ 8]*self[14] - self[10]*self[12],
            self[ 8]*self[15] - self[11]*self[12],
            self[ 9]*self[14] - self[10]*self[13],
            self[ 9]*self[15] - self[11]*self[13],
            self[10]*self[15] - self[11]*self[14]
        )


    def __determinantStep2(self, *args):
        return args[0]*args[11] - args[1]*args[10] + args[2]*args[9] + args[3]*args[8] - args[4]*args[7] + args[5]*args[6]


    def determinant(self):
        return self.__determinantStep2(*self.__determinantStep1())


    def inverse(self):
        return Matrix4(self).inverseInPlace()


    def inverseInPlace(self):
        a0, a1, a2, a3, a4, a5, b0, b1, b2, b3, b4, b5 = self.__determinantStep1()
        det = self.__determinantStep2(a0, a1, a2, a3, a4, a5, b0, b1, b2, b3, b4, b5)

        data = self.data()

        if math.fabs(det) > PRECISION:
            data[ 0] = + self[ 5]*b5 - self[ 6]*b4 + self[ 7]*b3
            data[ 4] = - self[ 4]*b5 + self[ 6]*b2 - self[ 7]*b1
            data[ 8] = + self[ 4]*b4 - self[ 5]*b2 + self[ 7]*b0
            data[12] = - self[ 4]*b3 + self[ 5]*b1 - self[ 6]*b0
            data[ 1] = - self[ 1]*b5 + self[ 2]*b4 - self[ 3]*b3
            data[ 5] = + self[ 0]*b5 - self[ 2]*b2 + self[ 3]*b1
            data[ 9] = - self[ 0]*b4 + self[ 1]*b2 - self[ 3]*b0
            data[13] = + self[ 0]*b3 - self[ 1]*b1 + self[ 2]*b0
            data[ 2] = + self[13]*a5 - self[14]*a4 + self[15]*a3
            data[ 6] = - self[12]*a5 + self[14]*a2 - self[15]*a1
            data[10] = + self[12]*a4 - self[13]*a2 + self[15]*a0
            data[14] = - self[12]*a3 + self[13]*a1 - self[14]*a0
            data[ 3] = - self[ 9]*a5 + self[10]*a4 - self[11]*a3
            data[ 7] = + self[ 8]*a5 - self[10]*a2 + self[11]*a1
            data[11] = - self[ 8]*a4 + self[ 9]*a2 - self[11]*a0
            data[15] = + self[ 8]*a3 - self[ 9]*a1 + self[10]*a0

            invDet = 1.0/det
            data[ 0] *= invDet
            data[ 1] *= invDet
            data[ 2] *= invDet
            data[ 3] *= invDet
            data[ 4] *= invDet
            data[ 5] *= invDet
            data[ 6] *= invDet
            data[ 7] *= invDet
            data[ 8] *= invDet
            data[ 9] *= invDet
            data[10] *= invDet
            data[11] *= invDet
            data[12] *= invDet
            data[13] *= invDet
            data[14] *= invDet
            data[15] *= invDet

            self.set(*data)
        else:
            self.setToIdentity()

        return self


    def orthogonalInPlace(self):
        data = Matrix3._orthogonalizeData(
                    [self[0], self[1], self[2],
                     self[4], self[5], self[6],
                     self[8], self[9], self[10]])
        self.__setRotationData(data)
        return self


    def orthogonal(self):
        return Matrix4(self).orthogonalInPlace()


    def rotateVector(self, vec):
        return Vector3(
            self[0]*vec[0] + self[4]*vec[1] + self[8] *vec[2],
            self[1]*vec[0] + self[5]*vec[1] + self[9] *vec[2],
            self[2]*vec[0] + self[6]*vec[1] + self[10]*vec[2]
        )


    def transformVector(self, vec):
        return Vector3(
            self[0]*vec[0] + self[4]*vec[1] + self[8] *vec[2] + self[12],
            self[1]*vec[0] + self[5]*vec[1] + self[9] *vec[2] + self[13],
            self[2]*vec[0] + self[6]*vec[1] + self[10]*vec[2] + self[14]
        )


    def setToIdentity(self):
        self.set(1.0, 0.0, 0.0, 0.0,
                 0.0, 1.0, 0.0, 0.0,
                 0.0, 0.0, 1.0, 0.0,
                 0.0, 0.0, 0.0, 1.0)


    def transpose(self):
        return Matrix4(self).transposeInPlace()


    def transposeInPlace(self):
        self.set(self[0], self[4], self[8], self[12],
                 self[1], self[5], self[9], self[13],
                 self[2], self[6], self[10], self[14],
                 self[3], self[7], self[11], self[15])
        return self


    def mirrorInPlace(self, center, normal, primaryAxis, secondaryAxis):
        self.__setRotationData(_mirrorAxis(self, normal, primaryAxis, secondaryAxis))

        tr = self.getPosition()
        self.setPosition((tr - center).mirror(normal) + center)

        return self


    def mirror(self, normal, primaryAxis, secondaryAxis):
        return Matrix3(self).mirrorInPlace(normal, primaryAxis, secondaryAxis)


    def lookAt(self, pointAt, normal, primaryAxis, secondaryAxis):
        position = self.getPosition()
        axis = _getAimAxis(pointAt-position, normal-position, primaryAxis, secondaryAxis)

        self.setRow(0, axis[0])
        self.setRow(1, axis[1])
        self.setRow(2, axis[2])


    def fastLookAt(self, pointAt, normal):
        position = self.getPosition()
        axis = _getFastAimAxis(pointAt-position, normal-position)

        self.setRow(0, axis[0])
        self.setRow(1, axis[1])
        self.setRow(2, axis[2])


    @staticmethod
    def createLookAt(position, pointAt, normal, primaryAxis, secondaryAxis):
        mat = Matrix4()
        mat.lookAt(pointAt-position, normal-position, primaryAxis, secondaryAxis)
        mat.setPosition(position)
        return mat




class Quaternion(Base):

    def __init__(self, *args):
        '''
        It accepts:
        * another Quaternion
        * a list of 4 float values
        * a Vector4
        * a Euler
        * a Matrix3
        * a Matrix4 (only rotation part)
        * a Vector3 and a float (axis and angle)
        * 3 floats as rotation x, y, z (in radians)
        '''

        alen = len(args)

        # Resolvable with super init

        if alen == 4:
            super(Quaternion, self).__init__(*args)
            return

        elif alen == 1 and isinstance(args[0], (Quaternion, Vector4, list, tuple)):
            super(Quaternion, self).__init__(*args[0])
            return

        # Initialised as default then see if post initialisation is required.

        super(Quaternion, self).__init__(0.0, 0.0, 0.0, 1.0)

        if alen:
            if alen == 3:
                self.fromEuler(args)

            elif alen == 2 and isinstance(args[0], Vector3) and isinstance(args[1], (int, float)):
                self.fromAxisAngle(args[0], args[1])

            elif isinstance(args[0], Euler):
               self.fromEuler(args[0])

            elif isinstance(args[0], Matrix3):
               self.fromMatrix3(args[0])

            elif isinstance(args[0], Matrix4):
               self.fromMatrix4(args[0])

            else:
                raise AttributeError('Impossible to set a Quaternion with this argument(s) - {} -'.format(args))

    #---------------------- Arithmetic operations -----------------------#

    def __neg__(self):
        return Quaternion(-self.x, -self.y, -self.z, -self.w)


    def __add__(self, other):
        return Quaternion(self.x + other.x, self.y + other.y, self.z + other.z, self.w + other.w)


    def __sub__(self, other):
        return Quaternion(self.x - other.x, self.y - other.y, self.z - other.z, self.w - other.w)


    def __mul__(self, other):
        if isinstance(other, Quaternion):
            av = Vector3(self.x, self.y, self.z)
            bv = Vector3(other.x, other.y, other.z)
            v = bv.cross(av) + (bv * self.w) + (av * other.w)
            rw = self.w * other.w - bv.dot(av)
            return Quaternion(v.x, v.y, v.z, rw)
        else:
            return Quaternion(self.x * other, self.y * other, self.z * other, self.w * other)


    def __div__(self, other):
        return self.__truediv__(other)


    def __truediv__(self, other):
        return Quaternion(self.x / other, self.y / other, self.z / other.z, self.w / other.w)

    #------------------------ Arithmetic updates ------------------------#

    def __iadd__(self, other):
        self.x += other.x
        self.y += other.y
        self.z += other.z
        self.w += other.w
        return self


    def __isub__(self):
        self.x -= self.x
        self.y -= self.y
        self.z -= self.z
        self.w -= other.w
        return self


    def __imul__(self, other):
        if isinstance(other, Quaternion):
            self.x *= other.x
            self.y *= other.y
            self.z *= other.z
            self.w *= other.w
        else:
            self.x *= other
            self.y *= other
            self.z *= other
            self.w *= other
        return self


    def __idiv__(self, other):
        return self.__itruediv__(other)


    def __itruediv__(self, other):
        if isinstance(other, Quaternion):
            self.__mul__(other.inverse())
        else:
            self.x /= other
            self.y /= other
            self.z /= other
            self.w /= other
        return self

    #--------------------------- Data access ----------------------------#

    @property
    def x(self):
        return self[0]

    @x.setter
    def x(self, value):
        self[0] = value


    @property
    def y(self):
        return self[1]

    @y.setter
    def y(self, value):
        self[1] = value


    @property
    def z(self):
        return self[2]

    @z.setter
    def z(self, value):
        self[2] = value


    @property
    def w(self):
        return self[3]

    @w.setter
    def w(self, value):
        self[3] = value

    #----------------------------- methods ------------------------------#

    def setToIdentity(self):
        self.set(0.0, 0.0, 0.0, 1.0)


    def getAxisX(self):
        return self.rotateVector(Vector3.XAXIS)


    def getAxisY(self):
        return self.rotateVector(Vector3.YAXIS)


    def getAxisZ(self):
        return self.rotateVector(Vector3.ZAXIS)


    def getAxis(axis):
        return self.rotateVector(axis.asVector3())


    def fromMatrix3(self, mat):
        # Code from Geometric Tools www.geometrictools.com
        # Algorithm in Ken Shoemake's article in 1987 SIGGRAPH course notes
        # article "HQuaternion Calculus and Fast Animation".

        _next = [1, 2, 0]

        trace = mat[0] + mat[4] + mat[8]

        if (trace > 0):
            # |w| > 1/2, may as well choose w > 1/2
            root = math.sqrt(trace + 1.0)  # 2w
            w = 0.5*root
            root = 0.5/root  # 1/(4w)
            self.x = (mat[5] - mat[7]) * root
            self.y = (mat[6] - mat[2]) * root
            self.z = (mat[1] - mat[3]) * root
        else:
            # |w| <= 1/2
            i = 0
            if (mat[4] > mat[0]):
                i = 1

            if (mat[8] > mat[i*3+i]):
                i = 2

            j = _next[i]
            k = _next[j]

            root = sqrt(mat[i*3+i] - mat[j*3+j] - mat[k*3+k] + 1.0)

            self[i] = 0.5*root
            root = 0.5/root
            w = (mat[j*3+k] - mat[k*3+j]) * root
            self[j] = (mat[i*3+j] + mat[j*3+i]) * root
            self[k] = (mat[i*3+k] + mat[k*3+i]) * root

        return self


    def fromMatrix4(self, mat):
        return self.fromMatrix3(mat.toMatrix3())


    def _toMatrixData(self):
        xx = 2.0 * self.x * self.x
        yy = 2.0 * self.y * self.y
        zz = 2.0 * self.z * self.z
        xy = 2.0 * self.x * self.y
        zw = 2.0 * self.z * self.w
        xz = 2.0 * self.x * self.z
        yw = 2.0 * self.y * self.w
        yz = 2.0 * self.y * self.z
        xw = 2.0 * self.x * self.w
        return [1.0-yy-zz, xy+zw,     xz-yw,
                xy-zw,     1.0-xx-zz, yz+xw,
                xz+yw,     yz-xw,     1.0-xx-yy]


    def toMatrix3(self):
        return Matrix3(self._toMatrixData())


    def toMatrix4(self):
        r = self._toMatrixData()
        return Matrix4(r[0], r[1], r[2], 0.0,
                       r[3], r[4], r[5], 0.0,
                       r[6], r[7], r[8], 0.0,
                        0.0,  0.0,  0.0, 1.0)


    def fromEuler(self, eulerAngles, rotationOrder=RotationOrder.XYZ):
        ''' Set this quaternion with the euler angles given.

        Args:
            eulerAngles: (Euler, list/tuple). The Euler angles, can be a Euler instance or an iterable of 3 floats.
                         When passing an iterable, the values must be expressed in *radians*.
            rotationOrder: The rotation order the Euler are expressed with. (default: {RotationOrder.XYZ})

        Raises:
            AttributeError: Raised when the eulerAngle attribute is not an Euler instance or list/tuple
        '''
        if isinstance(eulerAngles, Euler):
            # ensure euler is radians
            r = eulerAngles.toRadians();
            angleX, angleY, angleZ = eulerAngles.data()
        elif hasattr(eulerAngles, '__getitem__'):
            angleX, angleY, angleZ = eulerAngles
        else:
            raise AttributeError("the value passed to `fromEuler` is not a `Euler` instance or an object that can give back three floats (Vector3, list etc)"
                                 "value passed: {}".format(eulerAngles))

        XQuat = Quaternion(Vector3.XAXIS, angleX)
        YQuat = Quaternion(Vector3.YAXIS, angleY)
        ZQuat = Quaternion(Vector3.ZAXIS, angleZ)

        if rotationOrder == RotationOrder.XYZ:
            self.set(XQuat * (YQuat*ZQuat))
        elif rotationOrder == RotationOrder.XZY:
            self.set(XQuat * (ZQuat*YQuat))
        elif rotationOrder == RotationOrder.YXZ:
            self.set(YQuat * (XQuat*ZQuat))
        elif rotationOrder == RotationOrder.YZX:
            self.set(YQuat * (ZQuat*XQuat))
        elif rotationOrder == RotationOrder.ZXY:
            self.set(ZQuat * (XQuat*YQuat))
        elif rotationOrder == RotationOrder.ZYX:
            self.set(ZQuat * (YQuat*XQuat))


    def toEuler(self, rotationOrder=RotationOrder.XYZ):
        self.toMatrix3().toEuler(rotationOrder)


    def length(self):
        return math.sqrt(self.x*self.x + self.y*self.y + self.z*self.z + self.w*self.w)


    def squaredLength(self):
        return self.w*self.w + self.x*self.x + self.y*self.y + self.z*self.z


    def unitInPlace(self):
        leng = self.length()

        if isclose(leng, 0.0):
            self.set(0, 0, 0, 0) # becomes invalid
        else:
            self /= leng
        return self


    def unit(self):
        return Quaternion(self).unitInPlace()


    def dot(self, other):
        return self.x*other[0] + self.y*other[1] + self.z*other[2] + self.w*other[3];


    def conjugateInPlace(self):
        self.set(-self[0], -self[1], -self[2], self[3])
        return self


    def conjugate(self):
        return Quaternion(self).conjugateInPlace()


    def inverseInPlace(self):
        u = self.length()
        self.conjugateInPlace()
        self /= u
        return self


    def inverse(self):
        return Quaternion(self).inverseInPlace()


    def rotateVector(self, vec):
        pendingDeprecation('Quaternion.rotateVector is deprecated, please use Quaternion.transformVector instead')
        return self.transformVector(vec)


    def transformVector(self, vec):
        # from https://gamedev.stackexchange.com/questions/28395/rotating-vector3-by-a-quaternion

        # Extract the vector part of the quaternion
        vq = Vector3(q.x, q.y, q.z)
        # Extract the scalar part of the quaternion
        w = q.w
        # Compute the vector
        return 2.0 * vq.dot(vec) * vq + \
               (w*w - vq.dot(vq)) * vec + \
               2.0 * w * vq.cross(vec)


    def matchHemisphere(self, other):
        if self.dot(other) < 0.0:
            self.x = -self.x
            self.y = -self.y
            self.z = -self.z
            self.w = -self.w


    def exp(self):
        # If q = A*(x*i+y*j+z*k) where (x,y,z) is unit length, then
        # exp(q) = cos(A)+sin(A)*(x*i+y*j+z*k).  If sin(A) is near zero,
        # use exp(q) = cos(A)+A*(x*i+y*j+z*k) since A/sin(A) has limit 1.

        result = Quaternion()

        a = sqrt(x*x + y*y + z*z)

        sn = math.sin(a)
        result.w = cos(a)

        if math.fabs(sn) >= PRECISION:
            coeff = sn / a

            result.x = coeff * self.x
            result.y = coeff * self.y
            result.z = coeff * self.z
        else:
            result.x = self.x
            result.y = self.y
            result.z = self.z

        return result


    def log(self):
        # If q = cos(A)+sin(A)*(x*i+y*j+z*k) where (x,y,z) is unit length, then
        # log(q) = A*(x*i+y*j+z*k).  If sin(A) is near zero, use log(q) =
        # sin(A)*(x*i+y*j+z*k) since sin(A)/A has limit 1.

        result = QUaternion()
        result.w = 0

        if math.fabs(w) < 1.0:
            a = math.acos(w)
            sn = math.sin(a)

            if math.fabs(sn) >= PRECISION:
                coeff = a / sn
                result.x = coeff * self.x
                result.y = coeff * self.y
                result.z = coeff * self.z
                return result

        result.x = self.x
        result.y = self.y
        result.z = self.z
        result.w = self.w
        return result


    def slerp(self, q2, t, shortesPath=True):

        if self.dot(q2 < 0.0):
            q2 = -q2

        rq = self - q2
        rLen =  rq.length(rq)

        lq = self + q2
        lLen =  lq.length(lq)

        a = 2.0 * math.atan2(rLen, lLen)
        s = 1.0 - t

        return self * (sinXOverX(s * a) / sinXOverX(a) * s) + \
               q2 * (sinXOverX(t * a) / sinXOverX(a) * t)


    def slerpInPlace(self, q2, t, shortesPath=True):
        self.set( self.slerp(q2, t, shortesPath) )
        return self


    def fromAxisAngle(self, axis, angle):
        ''' Set this Quaternion to be the rotation around
            `axis` by the amount of `angle`

        Args:
            axis: (Vector3) Must be a *unit* Vector (normalized)
            angle: (float) The angle in *radians*
        '''
        halfAngle = angle * 0.5
        quatVec = axis * math.sin(halfAngle)
        self.w = math.cos(halfAngle)
        self.x = quatVec.x
        self.y = quatVec.y
        self.z = quatVec.z


    def toAxisAngle(self):
        ''' Convert this Quaternion to an axis and and angle (radians)

        Returns:
            The unit Vector3 representing the axis and a float representing the angle in radians.
            tuple
        '''
        sqrLength = self.x*self.x + self.y*self.y + self.z*self.z

        if isclose(sqrLength, 0.0):
            # Angle is 0 (mod 2*gmath::PI), so any axis will do.
            outAxis = Vector3(1, 0, 0)
            outAngle = 0.0

        else:
            invLength = 1.0 / math.sqrt(sqrLength)
            outAxis = Vector3(self.x*invLength, self.y*invLength, self.z*invLength)
            outAngle = 2.0 * math.acos(self.w)

        return outAxis, outAngle


    def mirrorInPlace(self, normal, primaryAxis, secondaryAxis):
        self.fromMatrix3(_mirrorAxis(self, normal, primaryAxis, secondaryAxis))
        return self


    def mirror(self, normal, primaryAxis, secondaryAxis):
        return Quaternion(self).mirrorInPlace(normal, primaryAxis, secondaryAxis)

    #--------------- deprecated, to be removed when safe ----------------#

    def normalizeInPlace(self):
        pendingDeprecation(
            'Quaternion.normalizeInPlace is deprecated, please use Quaternion.unitInPlace instead')

        return self.unitInPlace()


    def normalize(self):
        pendingDeprecation(
            'Quaternion.normalize is deprecated, please use Quaternion.unit instead')

        return self.unit()


    def rotateVector(self, vec):
        pendingDeprecation('Quaternion.rotateVector is deprecated, please use Quaternion.transformVector instead')
        return self.transformVector(vec)


    def setMatrix4(self, outMat, scale=[1, 1, 1], pos=[0, 0, 0]):
        pendingDeprecation('Quaternion.setMatrix4 is deprecated, please use Matrix4.fromQuaternion instead')

        outMat.formQuaternion(self)
        outMat.setPosition(scale)
        outMat.setScale(scale)




class Euler(Base):
    def __init__(self, *args):

        alen = len(args)

        if alen == 0:
            super(Euler, self).__init__(0.0, 0.0, 0.0)
            self.__unit = Unit.Radians

        elif alen == 3:
            super(Euler, self).__init__(*args)
            self.__unit = Unit.Radians

        elif alen == 4:
            super(Euler, self).__init__(args[0], args[1], args[2])
            self.__unit = Unit(args[3])

        elif alen == 1 and isinstance(args[0], Euler):
            super(Euler, self).__init__(*args[0])
            self.__unit = args[0].getUnit()

        elif alen == 1 and isinstance(args[0], (int, Unit)):
            super(Euler, self).__init__(0.0, 0.0, 0.0)
            self.__unit = Unit(args[0])

        elif alen == 1 and hasattr(args[0], '__getitem__'):
            super(Euler, self).__init__(*args[0])
            self.__unit = Unit.Radians

        elif alen == 2 and hasattr(args[0], '__getitem__') and isinstance(args[1], (int, Unit)):
            super(Euler, self).__init__(*args[0])
            self.__unit = Unit(args[1])

        else:
            raise TypeError('Impossible to set a Euler with this argument(s) - {} -'.format(args))

    #--------------------------- Data access ----------------------------#

    @property
    def x(self):
        return self[0]

    @x.setter
    def x(self, value):
        self[0] = value


    @property
    def y(self):
        return self[1]

    @y.setter
    def y(self, value):
        self[1] = value


    @property
    def z(self):
        return self[2]

    @z.setter
    def z(self, value):
        self[2] = value


    @property
    def w(self):
        return self[3]

    @w.setter
    def w(self, value):
        self[3] = value

    #----------------------------- Methods ------------------------------#

    def getUnit(self):
        return self.__unit


    def setUnit(self, inUnit):
        if self.__unit != inUnit:
            if inUnit is Unit.Degrees:
                self.x = toDegrees(self.x)
                self.y = toDegrees(self.y)
                self.z = toDegrees(self.z)
            else:
                self.x = toRadians(self.x)
                self.y = toRadians(self.y)
                self.z = toRadians(self.z)

        return self


    def toDegrees(self):
        return Euler(self).setUnit(Unit.Degrees)


    def toRadians(self):
        return Euler(self).setUnit(Unit.Radians)


    def toVector(self):
        return Vector3(self.x, self.y, self.z)



class Xfo(object):
    ''' The Xfo type represents a 3D transform.
        It uses a Vector3 for translation and scaling as well as a Quaternion for its rotation.
        It was inspired by FabricEngine's own Xfo type

        See Also: Quaternion, Vector3, Euler, Matrix4, Matrix3
    '''
    def __init__(self, *args):

        alen = len(*args)

        if alen == 0:
            self.__initData()

        elif alen == 1 and isinstance(args[0], Xfo):
            self.__initData(args[0].ori, args[0].tr, args[0].sc)

        elif alen == 1 and isinstance(args[0], Vector3):
            self.__initData(pos=args[0])

        elif alen == 1 and isinstance(args[0], Quaternion):
            self.__initData(args[0])

        elif alen == 1 and isinstance(args[0], Matrix3):
            self.__initData(args[0].toQuaternion(), scl=args[0].getScale())

        elif alen == 1 and isinstance(args[0], Matrix4):
            self.__initData(args[0].toQuaternion(), args[0].getPosition(), args[0].getScale())

        elif alen == 2 and isinstance(args[0], Vector3) and isinstance(args[1], Quaternion):
            pendingDeprecation('Initializing a Xfo with (Vector3, Quaternion) has been deprecated '
                               'in favour of (Quaternion, Vector3), for consistency.\n'
                               'Please use the other form')
            self.__initData(args[1], args[0])

        elif alen == 2 and isinstance(args[0], Quaternion) and isinstance(args[1], Vector3):
            self.__initData(args[0], args[1])

        elif alen == 3 and isinstance(args[0], Quaternion) and \
                           isinstance(args[1], Vector3) and \
                           isinstance(args[2], Vector3):
            self.__initData(args[0], args[1], args[2])

        elif alen == 9:
            sel.__initData(args[0:3], args[3:6], args[6:9])

        else:
            raise TypeError('Impossible to set a Euler with this argument(s) - {} -'.format(args))


    def __initData(self, ori=[0.0, 0.0, 0.0, 1.0], pos=[0.0, 0.0, 0.0], scl=[1.0, 1.0, 1.0]):
        self.__ori = Quaternion(ori)
        self.__tr = Vector3(pos)
        self.__sc = Vector3(scl)


    #---------------------- Arithmetic comparison                                                    s ----------------------#

    def __eq__(self, other):
        return self.__ori == other.ori and self.__tr == other.tr and self.__sc == other.sc


    def __neq__(self, other):
        return not self.__eq__(other)

    #---------------------- Arithmetic operations -----------------------#

    def __mul__(self, other):
        result = Xfo()
        result.tr  = other.transformVector(self.__tr)
        result.ori = self.__ori * other.ori
        result.sc  = self.__sc * other.sc


    def __imul__(self, other):
        self.__tr = other.transformVector(self.__tr)
        self.__ori *= other.ori
        self.__sc *= other.sc

    #--------------------------- Data access ----------------------------#

    @property
    def ori(self):
        return self.__ori

    @ori.setter
    def ori(self, value):
        self.__ori.set(value)


    @property
    def tr(self):
        return self.__tr

    @tr.setter
    def tr(self, value):
        self.__tr.set(value)


    @property
    def sc(self):
        return self.__sc

    @sc.setter
    def sc(self, value):
        self.__sc.set(value)

    #----------------------------- Methods ------------------------------#

    def setToIdentity(self):
        self.__ori.set(0.0, 0.0, 0.0, 1.0)
        self.__tr.set(0.0, 0.0, 0.0)
        self.__sc.set(1.0, 1.0, 1.0)


    def fromMatrix4(self, mat):
        self.__ori.fromMatrix4(mat)
        self.__tr.set(mat.getPosition())
        self.__sc.set(mat.getScale())


    def toMatrix4(self):
        mat = self.__ori.toMatrix4()
        mat.setPosition(self.__tr)
        mat.setScale(self.__sc)


    def transformVector(self, vec):
        # Trying XSI transformation sequence S-R-T
        return self.__ori.transformVector(vec * self.__sc) + self.__tr


    def inverseTransformVector(self, vec):
        return self.inverse().transformVector(vec)


    def inverse(self):
        return Xfo(self).inverseInPlace()


    def inverseInPlace(self):
        self.__ori.inverseInPlace()
        self.__sc.inverseInPlace()
        self._tr.set(self.__ori.rotateVector(-self.__tr * sc))

        return self


    def slerpInPlace(self, other, t, shortesPath=True):
        self.__ori.slerpInPlace(other.ori, t, shortesPath)
        self.__tr.linearInterpolateInPlace(other.tr, t)
        self.__sc.linearInterpolateInPlace(other.sc, t)
        return self


    def slerp(self, other, t, shortesPath=True):
        return Xfo(self).slerpInPlace(other, t, shortesPath)


    def distance(self, other):
        return self.__tr.distance(other.tr)


    def mirrorInPlace(self, center, normal, primary, secondary):
        self.__tr = (self.__tr - center).mirror(normal) + center
        self.__ori.mirrorInPlace(normal, primary, secondary)


    def mirror(self, normal, primaryAxis, secondaryAxis):
        return Xfo(self).mirrorInPlace(normal, primaryAxis, secondaryAxis)

    #---------------------------- Deprecated ----------------------------#

    def distanceTo(self, other):
        pendingDeprecation('Xfo.distanceTo is deprecated in favour of Xfo.distance, for consistency reasons')
        return self.distance(other)