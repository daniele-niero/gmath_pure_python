import sys

__all__ = ['DccExpand']


try:
    from maya import OpenMaya as OpenMaya1
    from maya.api import OpenMaya
    import six
    WITH_MAYA = True
except:
    WITH_MAYA = False



class DccExpand(type):
    def __init__(cls, classname, bases, classdict):
        super(DccExpand, cls).__init__(classname, bases, classdict)


        if WITH_MAYA:

            if cls.__name__ == 'Vector3':
                def fromMayaVector(self, mvec):
                    return self.set(mvec.x, mvec.y, mvec.z)

                def toMayaVector(self):
                    return OpenMaya.MVector(self.x, self.y, self.z)

                def toMaya1Vector(self):
                    return OpenMaya1.MVector(self.x, self.y, self.z)

                cls.fromMayaVector = fromMayaVector
                cls.toMayaVector = toMayaVector
                cls.toMaya1Vector = toMaya1Vector

            elif cls.__name__ == "Vector4":
                def fromMayaPoint(self, mpoint):
                    return self(mpoint.x, mpoint.y, mpoint.z, mpoint.w)

                def toMayaPoint(self):
                    return OpenMaya.MPoint(self.x, self.y, self.z, self.w)

                def toMaya1Point(self):
                    return OpenMaya1.MPoint(self.x, self.y, self.z, self.w)

                cls.fromMayaPoint = fromMayaPoint
                cls.toMayaPoint = toMayaPoint
                cls.toMaya1Point = toMaya1Point

            elif cls.__name__ == "Matrix3":
                def fromMayaMatrix(self, mmatrix):
                    if isinstance(mmatrix, OpenMaya.MMatrix):
                        elem = mmatrix.getElement
                    else:
                        elem = mmatrix

                    self.set(
                        elem(0, 0), elem(0, 1), elem(0, 2),
                        elem(1, 0), elem(1, 1), elem(1, 2),
                        elem(2, 0), elem(2, 1), elem(2, 2)
                    )

                def toMayaMatrix(self):
                    data = [self[0], self[1], self[2], 0.0,
                            self[3], self[4], self[5], 0.0,
                            self[6], self[7], self[8], 0.0,
                            0.0, 0.0, 0.0, 1.0]
                    return OpenMaya.MMatrix(data)

                def toMaya1Matrix(self):
                    data = [self[0], self[1], self[2], 0.0,
                            self[3], self[4], self[5], 0.0,
                            self[6], self[7], self[8], 0.0,
                            0.0, 0.0, 0.0, 1.0]
                    mmatrix = OpenMaya1.MMatrix()
                    OpenMaya1.MScriptUtil.createMatrixFromList(data, mmatrix)
                    return mmatrix

                cls.fromMayaMatrix = fromMayaMatrix
                cls.toMayaMatrix = toMayaMatrix
                cls.toMaya1Matrix = toMaya1Matrix

            elif cls.__name__ == "Matrix4":
                def fromMayaMatrix(self, mmatrix):
                    if isinstance(mmatrix, OpenMaya.MMatrix):
                        self.set(*mmatrix)
                    else:
                        self.set(
                            mmatrix(0, 0), mmatrix(0, 1), mmatrix(0, 2), mmatrix(0, 3),
                            mmatrix(1, 0), mmatrix(1, 1), mmatrix(1, 2), mmatrix(1, 3),
                            mmatrix(2, 0), mmatrix(2, 1), mmatrix(2, 2), mmatrix(2, 3),
                            mmatrix(3, 0), mmatrix(3, 1), mmatrix(3, 2), mmatrix(3, 3)
                        )

                def toMayaMatrix(self):
                    return OpenMaya.MMatrix(self)

                def toMaya1Matrix(self):
                    mmatrix = OpenMaya1.MMatrix()
                    OpenMaya1.MScriptUtil.createMatrixFromList(self.data(), mmatrix)
                    return mmatrix

                cls.fromMayaMatrix = fromMayaMatrix
                cls.toMayaMatrix = toMayaMatrix
                cls.toMaya1Matrix = toMaya1Matrix

            elif cls.__name__ == "Quaternion":
                def fromMayaQuaternion(self, mquat):
                    return self(mquat.x, mquat.y, mquat.z, mquat.w)

                def toMayaQuaternion(self):
                    return OpenMaya.MQuaternion(self.x, self.y, self.z, self.w)

                def toMaya1Quaternion(self):
                    return OpenMaya1.MQuaternion(self.x, self.y, self.z, self.w)

                cls.fromMayaQuaternion = fromMayaQuaternion
                cls.toMayaQuaternion = toMayaQuaternion
                cls.toMaya1Quaternion = toMaya1Quaternion

            elif cls.__name__ == "Euler":
                def fromMayaEuler(self, meuler):
                    return self.set(meuler.x, meuler.y, meuler.z)

                def toMayaEuler(self):
                    return OpenMaya.MRotationEuler(self.x, self.y, self.z)

                def toMaya1Euler(self):
                    return OpenMaya1.MRotationEuler(self.x, self.y, self.z)

                cls.fromMayaEuler = fromMayaEuler
                cls.toMayaEuler = toMayaEuler
                cls.toMaya1Euler = toMaya1Euler

            elif cls.__name__ == 'Xfo':
                def fromMayaMatrix(self, mmatrix):
                    gmatrix = Matrix4()
                    gmatrix.fromMayaMatrix(mmatrix)
                    self.fromMatrix4(gmatrix)

                def toMayaMatrix(self):
                    return self.toMatrix4().toMayaMatrix()

                def toMaya1Matrix(self):
                    return self.toMatrix4.toMaya1Matrix()


if WITH_MAYA:
    __all__ += ['getGlobalMatrix', 'getLocalMatrix', 'setGlobalMatrix', 'setLocalMatrix']
    import gmath

    def __mpathFromString(dagName):
        sl = OpenMaya.MSelectionList()
        sl.add(dagName)
        return sl.getDagPath(0)

    def __getMDagPath(arg):
        dagpath = None
        if isinstance(arg, (OpenMaya.MDagPath, OpenMaya1.MDagPath)):
            return arg
        elif isinstance(arg, six.string_types):
            return __mpathFromString(arg)
        elif hasattr(arg, '__apimdagpath__'): # this must be a pymel object
            return arg.__apimdagpath__()
        elif hasattr(arg, 'getDagPath'): # this must be a ggNodes object
            return arg.getDagPath()
        else:
            raise RuntimeError('unsupported object type')


    def getGlobalMatrix(mayaObj):
        path = __getMDagPath(mayaObj)
        gmatrix = gmath.Matrix4()
        gmatrix.fromMayaMatrix(path.inclusiveMatrix())
        return gmatrix


    def getLocalMatrix(mayaObj):
        path = __getMDagPath(mayaObj)
        matrix = path.inclusiveMatrix() * path.exclusiveMatrixInverse()
        gmatrix = gmath.Matrix4()
        gmatrix.fromMayaMatrix(matrix)
        return gmatrix


    def setGlobalMatrix(mayaObj, gmatrix, withUndo=False):
        '''
        Args:
            mayaObj (str|MDagPath|PyObject|GGNode): The target object
            gmatrix (GMath.Matrix4): The source Matrix
            withUndo (bool): if True this function will use Maya's commands instead of Maya's API.
                This allows an undo/redo pipeline but it can be slower.
                APIs should be in general faster but they come with no undo/redo pipeline (default: {False})
        '''
        path = __getMDagPath(mayaObj)

        if withUndo:
            parentGMatrix = gmath.Matrix4()
            parentGMatrix.fromMayaMatrix(path.exclusiveMatrixInverse())
            localMatrix = gmatrix * parentGMatrix

            objFullName = path.fullPathName()

            cmds.undoInfo(chunkName='GMathGlobalMatrixSet', openChunk=True)

            if path.apiType()==OpenMaya.MFn.kJoint:
                cmds.setAttr(objFullName+'.jointOrient', 0,0,0)
            cmds.xform(objFullName, matrix=localMatrix.data())

            cmds.undoInfo(chunkName='GMathGlobalMatrixSet', closeChunk=True)
        else:
            localMatrix = gmatrix.toMayaMatrix() * path.exclusiveMatrixInverse()
            mtmatrix = OpenMaya.MTransformationMatrix(localMatrix)
            mfnTransform = OpenMaya.MFnTransform(path)
            mfnTransform.setTransformation(mtmatrix)


    def setLocalMatrix(mayaObj, gmatrix, withUndo=False):
        '''
        Args:
            mayaObj (str|MDagPath|PyObject|GGNode): The target object
            gmatrix (GMath.Matrix4): The source Matrix
            withUndo (bool): if True this function will use Maya's commands instead of Maya's API.
                This allows an undo/redo pipeline but it can be slower.
                APIs should be in general faster but they come with no undo/redo pipeline (default: {False})
        '''
        path = __getMDagPath(mayaObj)
        if withUndo:
            cmds.undoInfo(chunkName='GMathGlobalMatrixSet', openChunk=True)

            objFullName = path.fullPathName()

            if path.apiType()==OpenMaya.MFn.kJoint:
                cmds.setAttr(objFullName+'.jointOrient', 0,0,0)
            cmds.xform(objFullName, matrix=gmatrix.data())

            cmds.undoInfo(chunkName='GMathGlobalMatrixSet', closeChunk=True)
        else:
            mtmatrix = OpenMaya.MTransformationMatrix(gmatrix.toMayaMatrix())
            mfnTransform = OpenMaya.MFnTransform(path)
            mfnTransform.setTransformation(mtmatrix)
